###
# ! - Just create new filles on this folder, and they are be loaded by init.sh script
#####

libPath=${rootProject}"/cli-lib"

loader()
{
    for file_name in $(ls ${path}); do
        if [[ ${file_name} != "init.sh" ]] && [[ ${file_name: -3} == ".sh" ]]; then
            source "$path/$file_name"
        fi
    done
}

load_essentials()
{
    path=${libPath}"/essentials"

    loader path
}

load_custom()
{
    path=${libPath}"/custom"

    loader path
}

load_scripts()
{
    # Load Essentials Libs
    load_essentials

    # Load Custom Libs :)
    load_custom
}

run()
{
    split_to_array "a" "com1 com2 com3"

    # for comm in $splittedCommand
    # do
    #     echo "> [$comm]"
    # done
}

args_manager()
{
    # Help page
    if [[ ${commands} == "" ]]; then
        ${helpFunction}
        exit
    fi

    # Run commands
    run
}

### - Start Scripts loading
load_scripts

### - Start Arguments execution
args_manager
