function init_colors() {
    prefix='\033[0;'

    black="${prefix}30m"
    red="${prefix}31m"
    green="${prefix}32m"
    yellow="${prefix}33m"
    blue="${prefix}34m"
    magenta="${prefix}35m"
    cyan="${prefix}36m"
    white="${prefix}37m"

    bgBlack="${prefix}300m"
    bgRed="${prefix}301m"
    bgGreen="${prefix}302m"
    bgYellow="${prefix}303m"
    bgBlue="${prefix}304m"
    bgMagenta="${prefix}305m"
    bgCyan="${prefix}306m"
    bgWhite="${prefix}307m"

    bold="${prefix}1m"

    cC='\033[0m'
}

init_colors
