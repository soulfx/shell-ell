split_to_array()
{
    separator=$1
    content=$2

    comArray=()

    for command in $(printf "$content"); do
        printf +1
        comArray+=($command)
    done

    printf "${comArray}[*]"

    # array_debug $comArray
}

# array_debug()
# {
#     arr=$1
#     for i in ${$arr[@]}
#     do
#         printf "index: $i - value: $arr[$i]"
#     done
# }
