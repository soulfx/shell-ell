#! /bin/bash

# Important Commands
rootProject=$(pwd)
commands=$@

### Custom Vars
helpFunction="default_help"
#

eval "ls ${rootProject} | grep main.sh"

if [ $? -eq 0 ]; then
    source "${rootProject}/cli-lib/init.sh"
else
    echo "You should run \"main.sh\" script on the same file folder."
fi
